package com.nextwave.utp.framework.configuration.propsfile;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Properties;

import javax.annotation.PreDestroy;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZooKeeperConfig extends GenericConfig implements Watcher
{
	private static Logger logger = LoggerFactory.getLogger( ZooKeeperConfig.class );
	private static ZooKeeperConfig zookeeperConfig;
	private boolean watchFlag;
	private ZooKeeper zooKeeper;
	private String environment;
	private String appname;
	private String myIp;
	private String serviceDetails;
	private String envServiceRoot;
	private String envConfigRoot;
	public static final String CONFIG_ROOT = "/configuration";
	public static final String SERVICE_ROOT = "/services";
	public static final String ENABLED = "enabled";
	public static final String DISABLED = "disabled";

	public synchronized static ZooKeeperConfig getZookeeperConfig(String fileLoc)
	{
		if( zookeeperConfig == null )
		{
			zookeeperConfig = new ZooKeeperConfig(fileLoc);
		}
		return zookeeperConfig;
		
	}
	private ZooKeeperConfig()
	{
		initZooKeeper("/home/orion/zookeeper.props");
		registerAsAServiceOnZK();
		loadPropertiesFromZookeeper();
	}
	private ZooKeeperConfig(String fileLoc)
	{
		initZooKeeper(fileLoc);
		registerAsAServiceOnZK();
		loadPropertiesFromZookeeper();
	}

	public boolean isWatchFlag()
	{
		return watchFlag;
	}

	public String getEnvServiceRoot()
	{
		return envServiceRoot;
	}

	public String getEnvironment()
	{
		return environment;
	}

	public String getAppname()
	{
		return appname;
	}

	private String getEnvConfigRoot()
	{
		return envConfigRoot;
	}

	/**
	 * @param path
	 * @param data
	 * @throws KeeperExceptionwatch
	 * @throws InterruptedException
	 */
	private void createNodeByte( String path, byte[] data ) throws KeeperException, InterruptedException
	{
		// TODO: Need to implement CREATOR_ALL_ACL for authentication
		zooKeeper.create( path, data, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT );
	}

	/**
	 * @param path
	 * @param data
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public void createNodeString( String path, String data ) throws KeeperException, InterruptedException
	{
		byte[] byteData = data.getBytes();
		Stat status = zooKeeper.exists( path, isWatchFlag() );
		if( status != null )
			updateDataAsByte( path, byteData );
		else
			createNodeByte( path, byteData );
	}

	/**
	 * @param path
	 * @return
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public String readString( String path ) throws KeeperException, InterruptedException
	{
		byte[] data = readByte( path );
		return new String( data );
	}

	/**
	 * @param path
	 * @param data
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public void updateDataAsString( String path, String data ) throws KeeperException, InterruptedException
	{
		byte[] byteData = data.getBytes();
		updateDataAsByte( path, byteData );
	}

	/**
	 * @param path
	 * @throws InterruptedException
	 * @throws KeeperException
	 */
	public void deleteNode( String path ) throws InterruptedException, KeeperException
	{
		Stat status = zooKeeper.exists( path, isWatchFlag() );
		zooKeeper.delete( path, status.getVersion() );
	}

	/**
	 * @param path
	 * @return
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public List< String > getChildren( String path ) throws KeeperException, InterruptedException
	{
		List< String > znodeList = zooKeeper.getChildren( path, isWatchFlag() );
		return znodeList;
	}

	/**
	 * @param path
	 * @return
	 * @throws KeeperException
	 * @throws InterruptedException
	 */
	public boolean exists( String path ) throws KeeperException, InterruptedException
	{
		if( zooKeeper.exists( path, isWatchFlag() ) != null )
			return true;
		else
			return false;
	}

	private byte[] readByte( String path ) throws KeeperException, InterruptedException
	{
		Stat status = zooKeeper.exists( path, isWatchFlag() );
		byte[] data = zooKeeper.getData( path, isWatchFlag(), status );
		return data;
	}

	private void updateDataAsByte( String path, byte[] data ) throws KeeperException, InterruptedException
	{
		Stat status = zooKeeper.exists( path, isWatchFlag() );
		zooKeeper.setData( path, data, status.getVersion() );
	}

	@PreDestroy
	public void cleanup()
	{
		if( zooKeeper == null || zooKeeper.getState() == ZooKeeper.States.CLOSED )
		{
			logger.debug( "-----------------------ZK already closed----------------------------" );
			return;
		}
		try
		{
			zooKeeper.close();
			logger.debug( "-------------------------Closed ZK connection------------------------" );
		}
		catch( Exception e )
		{
			logger.error( "Exception in cleanup.", e );
		}
	}

	public void deRegisterServiceFromZookeeper()
	{
		try
		{
			updateDataAsString( SERVICE_ROOT + "/" + environment + "/" + appname + "/" + myIp, DISABLED + "," + serviceDetails );
		}
		catch( Exception e )
		{
			logger.error( "Exception in deregistration of service.", e );
		}
	}

	public void setWatchFlag( boolean watchFlag )
	{
		this.watchFlag = watchFlag;
	}

	public Properties loadProperties()
	{
		Properties properties = new Properties();
		try
		{
			String appPath = "/" + environment + CONFIG_ROOT + "/" + appname;
			List< String > props = getChildren( appPath );
			logger.debug( props.toString() );
			for( String propName : props )
			{
				String propPath = appPath + "/" + propName;
				String propValue = readString( propPath );
				properties.put( propName, propValue );
			}
		}
		catch( Exception e )
		{
			logger.error( "Exception in zk load props:" + e );
		}
		return properties;
	}

	@Override
	public void process( WatchedEvent event )
	{
		try
		{
			logger.debug( "====Zookeeper properties changed===>>>" + event.toString() );
			if( event.getPath() != null && event.getPath().contains( getEnvConfigRoot() ) )
			{
				logger.debug( "Node changed:" + event.getPath() + " , " + event.getState() + " , " + event.toString() );
				if( watchFlag )
				{
					logger.debug( "Watchflag true, reloading properties" );
					reload();
				}
				else
				{
					logger.debug( "Watchflag false, not reloading properties" );
				}
			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	private String getPropertyName( String fullNodePath )
	{
		String propertyName = null;
		try
		{
			String tokens[] = fullNodePath.split( "/" );
			return tokens[tokens.length - 1];
		}
		catch( Exception e )
		{
			logger.error( "Exception in getPropertyName.", e );
		}
		return propertyName;
	}

	private void registerAsAServiceOnZK()
	{
		try
		{
			if( !exists( SERVICE_ROOT ) )
				createNodeString( SERVICE_ROOT, "services" );
			if( !exists( SERVICE_ROOT + "/" + environment ) )
				createNodeString( SERVICE_ROOT + "/" + environment, "environment" );
			if( !exists( SERVICE_ROOT + "/" + environment + "/" + appname ) )
				createNodeString( SERVICE_ROOT + "/" + environment + "/" + appname, "app name" );
			if( !exists( SERVICE_ROOT + "/" + environment + "/" + appname + "/" + myIp ) )
				createNodeString( SERVICE_ROOT + "/" + environment + "/" + appname + "/" + myIp, ENABLED + "," + serviceDetails );
			else
				updateDataAsString( SERVICE_ROOT + "/" + environment + "/" + appname + "/" + myIp, ENABLED + "," + serviceDetails );

		}
		catch( Exception e )
		{
			logger.error( "Exception in registerAsAServiceOnZK.", e );
		}
	}

	private void loadPropertiesFromZookeeper()
	{
		Properties properties = loadProperties();
		logger.debug( "Properties initialized:" + properties );
		setProperties( properties );
	}

	private void initZooKeeper(String fileLoc)
	{
		//final String CONFIG_FILE_PATH = "/home/deploy/bpl/conf/bpl-config.properties";
		final String CONFIG_FILE_PATH = fileLoc;
		InputStreamReader isr = null;
		try
		{
			isr = new InputStreamReader( new FileInputStream( new File( CONFIG_FILE_PATH ) ) );
		}
		catch( FileNotFoundException e1 )
		{
			e1.printStackTrace();
		}
		Properties props = new Properties();
		try
		{
			props.load( isr );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
		watchFlag = Boolean.parseBoolean( props.getProperty( "watchFlag", "true" ) );
		environment = props.getProperty( "environment", "test" );
		appname = props.getProperty( "appname", "authenticationservice" );
		serviceDetails = props.getProperty( "serviceDetails", "1234,abc" );
		myIp = null;

		String zkHost = props.getProperty( "zkHost", "localhost" );
		int sessionTimeout = Integer.parseInt( props.getProperty( "sessionTimeout", "10000" ) );

		try
		{
			InetAddress ipAddr = InetAddress.getLocalHost();
			myIp = ipAddr.getHostAddress();
		}
		catch( UnknownHostException ex )
		{
			ex.printStackTrace();
		}
		try
		{
			zooKeeper = new ZooKeeper( zkHost, sessionTimeout, this );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
		envServiceRoot = "/" + environment + SERVICE_ROOT + "/" + appname;
		envConfigRoot = "/" + environment + CONFIG_ROOT + "/" + appname;

		System.out.println( "watchFlag " + watchFlag + " zkHost " + zkHost + " sessionTimeout " + sessionTimeout + " environment " + environment + " appname " + appname + " serviceDetails "
				+ serviceDetails );
	}

	public void reload()
	{
		loadPropertiesFromZookeeper();
	}
}
