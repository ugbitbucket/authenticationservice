package com.nextwave.utp.framework.config;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

/**
 * <p>
 *     A contract which abstracts the implementation detail of a source providing
 *     configuration.
 *     <br/><br/>
 *     All configuration is meant to be key-value pairs.
 *     This interfaces contains various API for fetching an appropriate value type
 *     based on the configuration param key and a default value where applicable.
 * </p>
 *
 * @author Phaneendra Kumar Divi
 *
 * User: phani
 * Date: 18/10/11
 * Time: 12:38 PM
 */
@Component
public interface Configuration {
    /**
     * <p>
     * Fetches a string value for the key in question.
     * </p>
     * @param configParamKey key
     * @return null if not string value
     */
    public String getStringValue(String configParamKey);

    /**
     * <p>
     * Fetches a string value for the key in question.
     * If no value is fetched the default value is returned.
     * </p>
     * @param configParamKey key
     * @param defaultValue default value
     * @return default value if not string value
     */
    public String getStringValue(String configParamKey, String defaultValue);

    /**
     * <p>
     *     Fetches an array of strings from a comma separated list of string value
     *     against the key in question.
     *     If no value is fetched null is returned.
     * </p>
     * @param configParamKey key
     * @return null if not csv tokenized in to string array
     */
    public String[] getCSVStringValues(String configParamKey);

    /**
     * <p>
     *     Fetches an array of strings from a comma separated list of string value
     *     against the key in question.
     *     If no value is fetched null is returned.
     * </p>
     * @param configParamKey key
     * @param defaultValues default values
     * @return default values if not csv tokenized in to string array
     */
    public String[] getCSVStringValues(String configParamKey, String[] defaultValues);

    /**
     * <p>
     *     Fetches a set (no duplicates) of strings from a comma separated list of string value
     *     against the key in question.
     *     If no value is fetched null is returned.
     * </p>
     * @param configParamKey key
     * @return null if not csv tokenized in to string array
     */
    public Set<String> getCSVStringValuesAsSet(String configParamKey);

    /**
     * <p>
     *     Fetches a set of strings from a comma separated string of values
     *     against the key in question.
     *     If no value is fetched the default set is returned.
     * </p>
     * @param configParamKey key
     * @param defaultValues default values
     * @return default values if not csv as list
     */
    public Set<String> getCSVStringValuesAsSet(String configParamKey, Set<String> defaultValues);

    /**
     * <p>
     *     Fetches a list of strings from a comma separated list of string value
     *     against the key in question.
     *     If no value is fetched null is returned.
     * </p>
     * @param configParamKey key
     * @return null if not csv tokenized in to string array
     */
    public List<String> getCSVStringValuesAsList(String configParamKey);

    /**
     * <p>
     *     Fetches a list of strings from a comma separated list of string value
     *     against the key in question.
     *     If no value is fetched the default list is returned.
     * </p>
     * @param configParamKey key
     * @param defaultValues default values
     * @return default values if not csv as list
     */
    public List<String> getCSVStringValuesAsList(String configParamKey, List<String> defaultValues);

    /**
     * <p>
     * Fetches a boolean value for the key in question.
     * Returns Boolean.FALSE if no proper boolean value could be found or parsed.
     * </p>
     * @param configParamKey key
     * @return Boolean.FALSE if not boolean value
     */
    public boolean getBooleanValue(String configParamKey);

    /**
     * <p>
     * Fetches a boolean value for the key in question.
     * If no value is fetched the default value is returned.
     * </p>
     * @param configParamKey key
     * @param defaultValue default value
     * @return default value if not boolean value
     */
    public boolean getBooleanValue(String configParamKey, boolean defaultValue);

    /**
     * <p>
     * Fetches an integer value for the key in question.
     * Returns Integer.MIN_VALUE if no proper integer value could be found or parsed.
     * </p>
     * @param configParamKey key
     * @return Integer.MIN_VALUE if not integer value
     */
    public int getIntValue(String configParamKey);

    /**
     * <p>
     * Fetches an integer value for the key in question.
     * If no value is fetched the default value is returned.
     * </p>
     * @param configParamKey key
     * @param defaultValue default value
     * @return default value if not integer value
     */
    public int getIntValue(String configParamKey, int defaultValue);

    /**
     * <p>
     * Fetches a long value for the key in question.
     * Returns Long.MIN_VALUE if no proper long value could be found or parsed.
     * </p>
     * @param configParamKey key
     * @return Long.MIN_VALUE if not integer value
     */
    public long getLongValue(String configParamKey);

    /**
     * <p>
     * Fetches a long value for the key in question.
     * If no value is fetched the default value is returned.
     * </p>
     * @param configParamKey key
     * @param defaultValue default value
     * @return default value if not long value
     */
    public long getLongValue(String configParamKey, long defaultValue);

    /**
     * <p>
     * Fetches a float value for the key in question.
     * Returns Float.MIN_VALUE if no proper float value could be found or parsed.
     * </p>
     * @param configParamKey key
     * @return Float.MIN_VALUE if not float value
     */
    public float getFloatValue(String configParamKey);

    /**
     * <p>
     * Fetches a float value for the key in question.
     * If no value is fetched the default value is returned.
     * </p>
     * @param configParamKey key
     * @param defaultValue default value
     * @return default value if not float value
     */
    public float getFloatValue(String configParamKey, float defaultValue);

    /**
     * <p>
     * Fetches a double value for the key in question.
     * Returns Double.MIN_VALUE if no proper double value could be found or parsed.
     * </p>
     * @param configParamKey key
     * @return Double.MIN_VALUE if not float value
     */
    public double getDoubleValue(String configParamKey);

    /**
     * <p>
     * Fetches a double value for the key in question.
     * If no value is fetched the default value is returned.
     * </p>
     * @param configParamKey key
     * @param defaultValue default value
     * @return default value if not double value
     */
    public double getDoubleValue(String configParamKey, double defaultValue);

    /**
     * <p>
     * Fetches an enumerated value of the given type for the key in question.
     * Returns null if no proper value of the enumerated type could be found or parsed.
     * </p>
     *
     * @param configParamKey key
     * @param enumType type of the enumeration
     * @return null if not enumerated value of the given type
     */
    public <T extends Enum> T getEnumeratedValue(String configParamKey, Class<T> enumType);

    /**
     * <p>
     * Fetches an enumerated value of the given type for the key in question.
     * If no value is fetched the default value is returned.
     * </p>
     * @param configParamKey key
     * @param enumType type of the enumeration
     * @param defaultValue default value
     * @return default value if not enumerated value of given type
     */
    public <T extends Enum> T getEnumeratedValue(String configParamKey, Class<T> enumType, T defaultValue);
}
