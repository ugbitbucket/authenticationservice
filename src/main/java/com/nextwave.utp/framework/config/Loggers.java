package com.nextwave.utp.framework.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 *     Configuraiton related functionality specific logger instance.
 * </p>
 *
 * @author Phaneendra Kumar Divi
 * @since 26-10-2011 12:26
 */
public interface Loggers {
    public static final Logger CONFIGURATION_LOGGER = LoggerFactory.getLogger("CONFIGURATION_LOGGER");
}
