package com.nextwave.utp.authentication.service.dao.model;

public class PlayerFBDetails
{
	private Long fbuid;
	private String fbname;
	public Long getFbuid()
	{
		return fbuid;
	}
	public void setFbuid( Long fbuid )
	{
		this.fbuid = fbuid;
	}
	public String getFbname()
	{
		return fbname;
	}
	public void setFbname( String fbname )
	{
		this.fbname = fbname;
	}
	@Override
	public String toString()
	{
		return "PlayerFBDetails [fbuid=" + fbuid + ", fbname=" + fbname + "]";
	}
	
	

}
