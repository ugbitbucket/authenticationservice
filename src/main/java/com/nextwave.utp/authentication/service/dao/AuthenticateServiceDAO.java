package com.nextwave.utp.authentication.service.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.nextwave.utp.authentication.service.dao.model.PlayerDetails;
import com.nextwave.utp.authentication.service.dao.model.PlayerFBDetails;



@Component
public interface AuthenticateServiceDAO
{

	public boolean insertPlayerFBAndDeviceIdetails(PlayerDetails playerDet);
	
	public boolean updateLastLogin( String deviceId, String fbuid );
	
	public List<PlayerDetails> getPlayerDetailsList(String fbuid,String deviceId);
	
	public List<PlayerFBDetails> getFBNameList(List<String> fbList);
	
	
}
