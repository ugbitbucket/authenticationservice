package com.nextwave.utp.authentication.service.dao.model;

import java.sql.Timestamp;

public class PlayerDetails
{
          // DB mapping class
	private String deviceId;
	private String fbuid;
	private Timestamp lastLogin;
	private Timestamp recorded;
	
	
	public PlayerDetails( String deviceId, String fbuid, Timestamp lastLogin, Timestamp recorded )
	{
		this.deviceId = deviceId;
		this.fbuid = fbuid;
		this.lastLogin = lastLogin;
		this.recorded = recorded;
	}
	
	
	public String getDeviceId()
	{
		return deviceId;
	}
	public void setDeviceId( String deviceId )
	{
		this.deviceId = deviceId;
	}
	public String getFbuid()
	{
		return fbuid;
	}
	public void setFbuid( String fbuid )
	{
		this.fbuid = fbuid;
	}
	public Timestamp getLastLogin()
	{
		return lastLogin;
	}
	public void setLastLogin( Timestamp lastLogin )
	{
		this.lastLogin = lastLogin;
	}
	public Timestamp getRecorded()
	{
		return recorded;
	}
	public void setRecorded( Timestamp recorded )
	{
		this.recorded = recorded;
	}


	@Override
	public String toString()
	{
		return "PlayerDetails [deviceId=" + deviceId + ", fbuid=" + fbuid + ", lastLogin=" + lastLogin + ", recorded=" + recorded + "]";
	}
	
	
	
	
}
