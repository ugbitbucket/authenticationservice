package com.nextwave.utp.authentication.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Component;

import com.nextwave.utp.authentication.service.dao.AuthenticateServiceDAO;
import com.nextwave.utp.authentication.service.dao.model.PlayerDetails;
import com.nextwave.utp.authentication.service.dao.model.PlayerFBDetails;

@Component
public class AuthenticateServiceDAOImpl extends JdbcDaoSupport implements AuthenticateServiceDAO
{
	private static final Logger log = Logger.getLogger( AuthenticateServiceDAOImpl.class );	

	@Autowired
	public AuthenticateServiceDAOImpl( DataSource dataSource )
	{
		setDataSource( dataSource );		
		log.info( "   Datasource initialized....." );
	}

	public  boolean insertPlayerFBAndDeviceIdetails( PlayerDetails playerDet )
	{
		boolean success = false;
		String sqlQuery = "INSERT INTO fbid_versus_deviceId_det " + "(deviceid, fbuid, last_login) VALUES (?, ?, ?)";				
		log.info( "INSERT sql Query- :" + sqlQuery );
		System.out.println(  "INSERT sql Query- :" + sqlQuery+"   playerDet  : "+playerDet );
		try
		{
			Object[] params = new Object[] { playerDet.getDeviceId(), playerDet.getFbuid(), new Timestamp( System.currentTimeMillis())};
		        getJdbcTemplate().update(sqlQuery, params);	
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		return success;
	}

	public boolean updateLastLogin( String deviceId, String fbuid )
	{
		boolean success = false;

		try
		{
			// String sql = "update table fbid_versus_deviceId_det set last_login=now() where deviceid LIKE '" + deviceId + "' and fbuid LIKE '" + fbuid + "'";
			String sqlQuery = "update fbid_versus_deviceId_det set last_login=now()  where deviceid=? and fbuid=?";
			log.info( "UPDATE sql Query- :" + sqlQuery );
			System.out.println(  "UPDATE sql Query- :" + sqlQuery +"   deviceId : "+deviceId+"  fbuid :"+fbuid);
			int updateCnt = 0;
			try
			{
				Object[] params = new Object[] { deviceId, fbuid };
				updateCnt = getJdbcTemplate().update( sqlQuery, params );
				if( updateCnt > 0 )
				{
					success = true;
				}
			}
			catch( Exception e )
			{
				e.printStackTrace();
			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		return success;
	}

	public List<PlayerDetails> getPlayerDetailsList(String fbuid,String deviceId)
	{
		String sqlQuery = "SELECT * FROM fbid_versus_deviceId_det where deviceid=? or fbuid=?";
		Object[] params = new Object[] { deviceId, fbuid };
		List<PlayerDetails> playerList=new ArrayList<PlayerDetails>();
		try
		{			
			List< Map< String, Object > > rows=getJdbcTemplate().queryForList( sqlQuery, params );
			for( Map< String, Object > row : rows )
			{
				PlayerDetails details=new PlayerDetails((String)row.get( "deviceid" ),(String) row.get( "fbuid" ),(Timestamp) row.get( "last_login" ),(Timestamp) row.get( "recorded" ) );
				playerList.add( details );
			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		return playerList;
	}

	@Override
	public List< PlayerFBDetails > getFBNameList( List< String > fbList )
	{
		
		StringBuilder sqlQuery=new StringBuilder("select * from player_facebook where fbid in(");
		List<Long> fbidList=convert( fbList );
		List< PlayerFBDetails > fblist=new ArrayList< PlayerFBDetails >();
		for( int i=0;i<fbidList.size();i++)
		{
			sqlQuery.append( fbidList.get( i ));
			if(i!=fbidList.size()-1){
				sqlQuery.append( ",");	
			}
			
		}
		sqlQuery.append( ")" );		
		try
		{			
			List< Map< String, Object > > rows=getJdbcTemplate().queryForList( sqlQuery.toString() );
			for( Map< String, Object > row : rows )
			{
				PlayerFBDetails fbDet=new PlayerFBDetails();
				fbDet.setFbuid((Long)row.get( "fbid" ));
				fbDet.setFbname( (String)row.get( "fb_name" ));
				fblist.add( fbDet );
			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		return fblist;
	}
	
	public List<Long> convert(List< String > fbList)
	{
		List< Long > fbIdList = new ArrayList<Long>();
		try
		{
			for( String fbid : fbList )
			{
				fbIdList.add( Long.parseLong( fbid ));
			}
		}
		catch( Exception e )
		{
			
			e.printStackTrace();
		}
		return fbIdList;
	}
	
	
	
	
	

}
