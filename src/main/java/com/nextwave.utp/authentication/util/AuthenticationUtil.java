package com.nextwave.utp.authentication.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nextwave.utp.authentication.service.dao.AuthenticateServiceDAO;
import com.nextwave.utp.authentication.service.dao.model.PlayerDetails;
import com.nextwave.utp.cache.modelObject.PlayerFBAndDeviceDet;
import com.nextwave.utp.cache.modelObject.UserDetails;
import com.nextwave.utp.redisCacheManger.RedisGetAndPutUtil;

@Component
public class AuthenticationUtil
{
	private static final Logger log = Logger.getLogger( AuthenticationUtil.class );
	@Autowired
	private RedisGetAndPutUtil util;
	@Autowired
	AuthenticateServiceDAO authenticationServiceDAO;
	@Autowired
	private com.nextwave.utp.framework.config.Configuration config;

	// getting player FBUID list on deviceId key from redis server and
	// converting
	public List< UserDetails > getFBuidListOnDeviceIdKeyFromRedis( String deviceIdkey )
	{
		try
		{
			List< UserDetails > userDetList = new ArrayList< UserDetails >();
			List< Object > list = util.getObjectListFromRedis( deviceIdkey );
			if( list != null && !list.isEmpty() )
			{
				return userDetList;
			}
			else if( list == null || list.isEmpty() )
			{
				return null;
			}
		}
		catch( Exception e )
		{

			e.printStackTrace();
		}
		return null;
	}

	// getting player deviceId list on fbuidkey from redis server and
	// converting
	public List< UserDetails > getIdListOnKeyFromRedis( String key )
	{
		try
		{
			List< UserDetails > userDetList = new ArrayList< UserDetails >();
			List< Object > list = util.getObjectListFromRedis( key );
			if( list != null && !list.isEmpty() ) // found in Redis
			{
				for( Object object : list )
				{
					UserDetails userDet = ( UserDetails ) object;
					userDetList.add( userDet );
				}
				return userDetList;
			}
			else if( list == null || list.isEmpty() )
			{
				return null;
			}
		}
		catch( Exception e )
		{

			e.printStackTrace();
		}
		return null;
	}

	public boolean isDeviceIdAvailableInDeviceList( String fbuidkey, String deviceId, List< UserDetails > userList )
	{
		boolean isDeviceIdAvailable = false;
		try
		{
			for( UserDetails userDetails : userList )
			{
				if( userDetails.getId().equals( deviceId ) )
				{
					isDeviceIdAvailable = true;
				}
			}
			return isDeviceIdAvailable;
		}
		catch( Exception e )
		{

			e.printStackTrace();
			return isDeviceIdAvailable;
		}

	}

	public boolean isDeviceIdAvailableInFBList( String deviceIdKey, String fbuid, List< UserDetails > userList )
	{
		boolean isDeviceIdAvailable = false;
		try
		{
			for( UserDetails userDetails : userList )
			{
				if( userDetails.getId().equals( fbuid ) )
				{
					isDeviceIdAvailable = true;
				}
			}
			return isDeviceIdAvailable;
		}
		catch( Exception e )
		{

			e.printStackTrace();
			return isDeviceIdAvailable;
		}

	}

	public PlayerFBAndDeviceDet checkAndGetPlayerFBAndDeviceDetails( String fbuidKey, String deviceId )
	{
		boolean isLoginAllowed = false;
		PlayerFBAndDeviceDet pAndDeviceDet=null;
		try
		{
			final int MAX_DEVICE_ID_LIMIT = config.getIntValue( "MAX_DEVICE_ID_LIMIT", 3 );
			final int MAX_FBUID_LIMIT = config.getIntValue( "MAX_FBUID_LIMIT", 3 );
			List< UserDetails > redisDeviceidList = getIdListOnKeyFromRedis( fbuidKey );
			List< UserDetails > redisFbuidList = getIdListOnKeyFromRedis( deviceId );
			System.out.println( "MAX_FBUID_LIMIT :" + MAX_FBUID_LIMIT );
			System.out.println( "MAX_DEVICE_ID_LIMIT :" + MAX_DEVICE_ID_LIMIT );
			System.out.println( "Before All Cache check : redisDeviceidList :" + redisDeviceidList + "  redisFbuidList :" + redisFbuidList );
			
			// if Cache does not contains DATA
			if( redisDeviceidList == null || redisFbuidList == null )
			{
				System.out.println( "Cache does not contains DATA" + " fbuidKey :" + fbuidKey + "  deviceId :" + deviceId );
				List< PlayerDetails > playerDetList = authenticationServiceDAO.getPlayerDetailsList( fbuidKey, deviceId );
				System.out.println( "DB Hit---" + playerDetList );
				Map< String, List< String > > keyVersusListOfIDMap = null;
				List< String > deviceIdList = null;
				List< String > fbuIdList = null;				
				boolean isNewUser = true;
				for( int i = 0; i < playerDetList.size(); i++ )
				{
					PlayerDetails details = playerDetList.get( i );
					if( details.getFbuid() == fbuidKey && details.getDeviceId() == deviceId )
					{
						isNewUser = false;
					}
				}

				

				keyVersusListOfIDMap = getKeySpecificIDList( playerDetList, fbuidKey, deviceId );
				if( keyVersusListOfIDMap != null )
				{
					deviceIdList = keyVersusListOfIDMap.get( fbuidKey );
					fbuIdList = keyVersusListOfIDMap.get( deviceId );
				}
				System.out.println( "Is new User ? " + isNewUser+"   deviceIdList.size() :"+deviceIdList.size()+"  fbuIdList.size() : "+fbuIdList.size() );
				
				if( ( deviceIdList != null && deviceIdList.size() < MAX_DEVICE_ID_LIMIT ) && ( fbuIdList != null && fbuIdList.size() < MAX_FBUID_LIMIT ) )
				{
					isLoginAllowed = true;

					if( isNewUser )
					{
						System.out.println( "Inserted in DB" );
						insertNewUserDetails( fbuidKey, deviceId );
						 pAndDeviceDet=getPlayerFBAndDeviceDet(deviceIdList,fbuIdList,isLoginAllowed);
						return pAndDeviceDet;
					}
				}
                               if(isLoginAllowed)
				{
					util.putListOfDeviceIdAgainstFbuidKeyIntoRedis( fbuidKey, playerDetList, deviceId );
					util.putListOfFBuIdAgainstDeviceIdKeyIntoRedis( deviceId, playerDetList, fbuidKey );
					updateUserLastLogin( fbuidKey, deviceId );
				}
                                pAndDeviceDet=getPlayerFBAndDeviceDet(deviceIdList,fbuIdList,isLoginAllowed);
                                System.out.println( "pAndDeviceDet while update :"+pAndDeviceDet );
                               return pAndDeviceDet;
			}
			else
			{
				// if Cache contain User DATA
				System.out.println( "Case 1 Cache  contains DATA" + " fbuidKey :" + fbuidKey + "  deviceId :" + deviceId );
				System.out.println( "Case 1 Redis DeviceId List :" + redisDeviceidList + "   Redis  FbuidList : " + redisFbuidList );
				if( redisDeviceidList.size()  > MAX_DEVICE_ID_LIMIT || redisFbuidList.size() > MAX_FBUID_LIMIT )
				{
					
					isLoginAllowed = false;
					pAndDeviceDet=getPlayerFBAndDeviceDetCache(redisDeviceidList,redisFbuidList,isLoginAllowed);
					return pAndDeviceDet;
				}
				boolean isAvailableInCache = false;
				System.out.println( "Case 1 redisDeviceidList :" + redisDeviceidList + "  deviceId : " + deviceId );
				for( UserDetails userDetails : redisDeviceidList )
				{
					System.out.println( "Case 1 first   :" + userDetails + "  deviceId :" + deviceId );
					if( userDetails.getId().equals( deviceId ) )
					{

						isAvailableInCache = true;
						System.out.println( "Matched....Matched case 1 :" + isAvailableInCache );
						break;
					}
				}
				if( isAvailableInCache )
				{
					isAvailableInCache = false;
					System.out.println( "Case 1 redisFbuidList :" + redisFbuidList + "  fbuidKey : " + fbuidKey );
					for( UserDetails userDetails : redisFbuidList )
					{

						if( userDetails.getId().equals( fbuidKey ) )
						{
							isAvailableInCache = true;
							System.out.println( "Matched....Matched case 2 :" + isAvailableInCache );
							break;
						}
					}
				}

				System.out.println( "case 1  isAvailableInCache  :" + isAvailableInCache );
				if( !isAvailableInCache )
				{
					System.out.println( "Case 2 not Available In Cache   contains DATA" + " fbuidKey :" + fbuidKey + "  deviceId :" + deviceId );
					System.out.println( "Case 2 Redis DeviceId List :" + redisDeviceidList + "   Redis  FbuidList : " + redisFbuidList );
					List< PlayerDetails > playerDetList = authenticationServiceDAO.getPlayerDetailsList( fbuidKey, deviceId );
					boolean isAvailableInDB = false;
					
					Map< String, List< String >> keyVersusListOfIDMap = null;
					if( playerDetList != null && playerDetList.size() > 0 )
					{
						keyVersusListOfIDMap = getKeySpecificIDList( playerDetList, fbuidKey, deviceId );
						if( keyVersusListOfIDMap != null )
						{
							List< String > deviceIdList = keyVersusListOfIDMap.get( fbuidKey );
							List< String > fbuIdList = keyVersusListOfIDMap.get( deviceId );
							if( ( deviceIdList != null && deviceIdList.size()  > MAX_DEVICE_ID_LIMIT ) || ( fbuIdList != null && fbuIdList.size()  > MAX_FBUID_LIMIT ) )
							{
								
								isLoginAllowed = false;
								 pAndDeviceDet=getPlayerFBAndDeviceDetCache(redisDeviceidList,redisFbuidList,isLoginAllowed);
								return pAndDeviceDet;
							}
						}

						for( PlayerDetails playerDetails : playerDetList )
						{

							if( playerDetails.getDeviceId().equals( deviceId ) && playerDetails.getFbuid().equals( fbuidKey ) )
							{
								isAvailableInDB = true;
								break;
							}
						}
					}

					if( !isAvailableInDB && keyVersusListOfIDMap != null )
					{
						List< String > deviceIdList = keyVersusListOfIDMap.get( fbuidKey );
						List< String > fbuIdList = keyVersusListOfIDMap.get( deviceId );
						
						if( deviceIdList != null && deviceIdList.size() <= MAX_DEVICE_ID_LIMIT && fbuIdList != null && fbuIdList.size() <= MAX_FBUID_LIMIT )
						{
							insertNewUserDetails( fbuidKey, deviceId );
						}
						else
						{
							isLoginAllowed = false;	
							pAndDeviceDet=getPlayerFBAndDeviceDet(deviceIdList,fbuIdList,isLoginAllowed);
							return pAndDeviceDet;
						}
					}
					else if( isAvailableInDB )
					{
						isLoginAllowed = true;
						util.putListOfDeviceIdAgainstFbuidKeyIntoRedis( fbuidKey, playerDetList, deviceId );
						util.putListOfFBuIdAgainstDeviceIdKeyIntoRedis( deviceId, playerDetList, fbuidKey );
						keyVersusListOfIDMap = getKeySpecificIDList( playerDetList, fbuidKey, deviceId );
						List< String > deviceIdList =null;
						List< String > fbuIdList =null;
						
						if( keyVersusListOfIDMap != null )
						{
							 deviceIdList = keyVersusListOfIDMap.get( fbuidKey );
							 fbuIdList = keyVersusListOfIDMap.get( deviceId );							
							if( ( deviceIdList != null && deviceIdList.size()  > MAX_DEVICE_ID_LIMIT ) || ( fbuIdList != null && fbuIdList.size() > MAX_FBUID_LIMIT ) )
							{
								isLoginAllowed = false;
								 pAndDeviceDet=getPlayerFBAndDeviceDet(deviceIdList,fbuIdList,isLoginAllowed);
								return pAndDeviceDet;
							}
						}
						 pAndDeviceDet=getPlayerFBAndDeviceDet(deviceIdList,fbuIdList,isLoginAllowed);
						return pAndDeviceDet;
					}

				}
				else
				{
					isLoginAllowed = true;
					List< Object > deviceIdList = util.getObjectListFromRedis( fbuidKey );
					List< Object > fbuIdList = util.getObjectListFromRedis( deviceId );
					int deviceCnt=0;
					int fbCnt=0;
					if(fbuIdList!=null)
					{
						fbCnt=fbuIdList.size();
					}
					if(deviceIdList!=null)
					{
						deviceCnt=deviceIdList.size();
					}
					Map<String,String> msgMap=getTextAndImageURL(fbCnt, deviceCnt);
					String text=msgMap.get( "text" );
					String imageURL=msgMap.get( "imageURL" );
					 pAndDeviceDet=new PlayerFBAndDeviceDet( fbCnt, deviceCnt, imageURL, 0, text, MAX_FBUID_LIMIT, MAX_DEVICE_ID_LIMIT, isLoginAllowed );	
					if( deviceIdList != null && deviceIdList.size()  > MAX_DEVICE_ID_LIMIT && fbuIdList != null && fbuIdList.size() > MAX_FBUID_LIMIT )
					{
						isLoginAllowed = false;
						pAndDeviceDet.setLoginAllowed( isLoginAllowed );
						return pAndDeviceDet;
					}
					
					updateUserLastLogin( fbuidKey, deviceId );
					return pAndDeviceDet;
				}

			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		return pAndDeviceDet;

	}

	public void insertNewUserDetails( String fbuidKey, String deviceId )
	{
		try
		{
			System.out.println( "insertNewUserDetails...." );
			PlayerDetails playerDet = new PlayerDetails( deviceId, fbuidKey, new Timestamp( System.currentTimeMillis() ), new Timestamp( System.currentTimeMillis() ) );
			authenticationServiceDAO.insertPlayerFBAndDeviceIdetails( playerDet );
			System.out.println( "inside CheckUtil put into redis : FB Key --" + fbuidKey );
			System.out.println( "inside CheckUtil put into redis : DEvice Key --" + deviceId );
			util.putDeviceIdAndFbuidKeyIntoRedis( fbuidKey, deviceId );
			util.putFbuidAndDeviceIdKeyIntoRedis( deviceId, fbuidKey );
			log.info( "inserted new records into DB...fbuidkey :" + fbuidKey + "  deviceId : " + deviceId );

		}
		catch( Exception e )
		{
			e.printStackTrace();
		}

	}

	public void updateUserLastLogin( String fbuidKey, String deviceId )
	{
		try
		{
			boolean status = authenticationServiceDAO.updateLastLogin( deviceId, fbuidKey );
			System.out.println( "updateUserLastLogin---" + "   fbuidKey :" + fbuidKey + " deviceId : " + deviceId + " Status :" + status );
			if( status )
			{
				util.updateLastLoginInRedisOnFBKey( fbuidKey, deviceId );
				util.updateLastLoginInRedisOnDeviceKey( deviceId, fbuidKey );
			}
			List< UserDetails > redisFBidList = getIdListOnKeyFromRedis( fbuidKey );
			List< UserDetails > redisDeviceidList = getIdListOnKeyFromRedis( deviceId );
			System.out.println( "After Updation ...redisFBidList  :" + redisFBidList + "   redisDeviceidList :" + redisDeviceidList );
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	public Map< String, List< String >> getKeySpecificIDList( List< PlayerDetails > playerDetList, String fbuidKey, String deviceIdKey )
	{
		Map< String, List< String >> keyIdListMap = new HashMap<>();
		try
		{
			List< String > deviceIdList = new ArrayList< String >();
			List< String > fbuIdList = new ArrayList< String >();

			if( playerDetList != null )
			{
				if( playerDetList.size() == 0 )
				{
					deviceIdList.add( deviceIdKey );
					fbuIdList.add( fbuidKey );
				}
				else
				{
					for( PlayerDetails playerDetails : playerDetList )
					{
						if( playerDetails.getFbuid().equals( fbuidKey ) )
						{
							deviceIdList.add( playerDetails.getDeviceId() );
						}
						if( playerDetails.getDeviceId().equals( deviceIdKey ) )
						{
							fbuIdList.add( playerDetails.getFbuid() );
						}
					}

				}

				keyIdListMap.put( fbuidKey, deviceIdList );
				keyIdListMap.put( deviceIdKey, fbuIdList );

				return keyIdListMap;
			}

		}
		catch( Exception e )
		{
			e.printStackTrace();
		}

		return null;
	}
	
	public PlayerFBAndDeviceDet getPlayerFBAndDeviceDet(List< String > deviceIdList,List< String > fbList,boolean isLoginAllowed)
	{
		final int MAX_DEVICE_ID_LIMIT = config.getIntValue( "MAX_DEVICE_ID_LIMIT", 3 );
		final int MAX_FBUID_LIMIT = config.getIntValue( "MAX_FBUID_LIMIT", 3 );
		final int nfbLimit_startWarning_cnt=config.getIntValue("nfbLimit_startWarning_cnt");
		final int nDeviceLimit_startWarning_cnt=config.getIntValue("nDeviceLimit_startWarning_cnt");
		PlayerFBAndDeviceDet playerFBAndDeviceDet=new PlayerFBAndDeviceDet();
		playerFBAndDeviceDet.setFbThresholdVal( MAX_FBUID_LIMIT );
		playerFBAndDeviceDet.setDeviceThresholdVal( MAX_DEVICE_ID_LIMIT );
		playerFBAndDeviceDet.setLoginAllowed( isLoginAllowed );		
		 String text ="";
		String imageURL="";
		if(deviceIdList!=null)
		{
			playerFBAndDeviceDet.setDeviceIdCnt( deviceIdList.size() );
		}
		if(fbList!=null)
		{
			playerFBAndDeviceDet.setFbCnt( fbList.size() );
		}
		if(playerFBAndDeviceDet.getFbCnt()>nfbLimit_startWarning_cnt)
		{			
			text = "This device was already logged in using currentFBIDCount different Facebook accounts,You can login with maximum of only " + playerFBAndDeviceDet.getFbCnt() + " accounts";
			imageURL = "You can login with this Facebook account in a maximum of " + MAX_DEVICE_ID_LIMIT + " devices";
			
		}
		else if( playerFBAndDeviceDet.getDeviceIdCnt()>nDeviceLimit_startWarning_cnt)
		{
			text = "This Facebook account was already associated with " + playerFBAndDeviceDet.getDeviceIdCnt() + " devices.";			
			imageURL = "You can login with this Facebook account in a maximum of "+ MAX_FBUID_LIMIT + " devices";
			
		}
		playerFBAndDeviceDet.setText(text );		
		playerFBAndDeviceDet.setImageURL(  imageURL);
		return playerFBAndDeviceDet;
	}

	public PlayerFBAndDeviceDet getPlayerFBAndDeviceDetCache(List< UserDetails > deviceIdList,List< UserDetails > fbList,boolean isLoginAllowed)
	{
		final int MAX_DEVICE_ID_LIMIT = config.getIntValue( "MAX_DEVICE_ID_LIMIT", 3 );
		final int MAX_FBUID_LIMIT = config.getIntValue( "MAX_FBUID_LIMIT", 3 );
		final int nfbLimit_startWarning_cnt=config.getIntValue("nfbLimit_startWarning_cnt");
		final int nDeviceLimit_startWarning_cnt=config.getIntValue("nDeviceLimit_startWarning_cnt");
		PlayerFBAndDeviceDet playerFBAndDeviceDet=new PlayerFBAndDeviceDet();
		playerFBAndDeviceDet.setFbThresholdVal( MAX_FBUID_LIMIT );
		playerFBAndDeviceDet.setDeviceThresholdVal( MAX_DEVICE_ID_LIMIT );
		playerFBAndDeviceDet.setLoginAllowed( isLoginAllowed );
		String text="", imageURL = "";
		if(deviceIdList!=null)
		{
			playerFBAndDeviceDet.setDeviceIdCnt( deviceIdList.size() );
		}
		if(fbList!=null)
		{
			playerFBAndDeviceDet.setFbCnt( fbList.size() );
		}
		
		if(playerFBAndDeviceDet.getFbCnt()>nfbLimit_startWarning_cnt)
		{			
			text = "This device was already logged in using currentFBIDCount different Facebook accounts,You can login with maximum of only " + playerFBAndDeviceDet.getFbCnt() + " accounts";
			imageURL = "You can login with this Facebook account in a maximum of " + MAX_DEVICE_ID_LIMIT + " devices";
			
		}
		else if( playerFBAndDeviceDet.getDeviceIdCnt()>nDeviceLimit_startWarning_cnt)
		{
			text = "This Facebook account was already associated with " + playerFBAndDeviceDet.getDeviceIdCnt() + " devices.";			
			imageURL = "You can login with this Facebook account in a maximum of "+ MAX_FBUID_LIMIT + " devices";
			
		}
		System.out.println( "Response Sent is " + playerFBAndDeviceDet );
		playerFBAndDeviceDet.setImageURL( imageURL );
		playerFBAndDeviceDet.setText( text );
		return playerFBAndDeviceDet;
	}
	
	public Map<String,String> getTextAndImageURL(int fbCnt,int deviceIdCnt)
	{
		final int nfbLimit_startWarning_cnt=config.getIntValue("nfbLimit_startWarning_cnt");
		final int nDeviceLimit_startWarning_cnt=config.getIntValue("nDeviceLimit_startWarning_cnt");
		final int MAX_DEVICE_ID_LIMIT = config.getIntValue( "MAX_DEVICE_ID_LIMIT", 3 );
		final int MAX_FBUID_LIMIT = config.getIntValue( "MAX_FBUID_LIMIT", 3 );
		
		Map<String,String> msgMap=new HashMap<String,String>();
                String text ="";
		String imageURL="";
                 try
		{		
			if(fbCnt>nfbLimit_startWarning_cnt)
			{			
				text = "This device was already logged in using currentFBIDCount different Facebook accounts,You can login with maximum of only " + fbCnt + " accounts";
				imageURL = "You can login with this Facebook account in a maximum of " + MAX_DEVICE_ID_LIMIT + " devices";
				
			}
			else if( deviceIdCnt>nDeviceLimit_startWarning_cnt)
			{
				text = "This Facebook account was already associated with " + deviceIdCnt + " devices.";			
				imageURL = "You can login with this Facebook account in a maximum of "+ MAX_FBUID_LIMIT + " devices";
				
			}
		}
		catch( Exception e )
		{
			
			e.printStackTrace();
		}
                 msgMap.put( "text", text );
                 msgMap.put( "imageURL", imageURL );
                 System.out.println( "Message Map Sent is " + msgMap );
                 return msgMap;
                 
                 
	}
}
