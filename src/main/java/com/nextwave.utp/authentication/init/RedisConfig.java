package com.nextwave.utp.authentication.init;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisPoolConfig;



@Component
@Order(2)
@org.springframework.context.annotation.Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport
{
	private static final Logger log = Logger.getLogger(RedisConfig.class);
         @Autowired
	private com.nextwave.utp.framework.config.Configuration config;
         
         @Bean( name = "jedisPoolConfig" )
         JedisPoolConfig jedisPoolConfig() {

             JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();             
             jedisPoolConfig.setMaxTotal(10000);
             jedisPoolConfig.setMinIdle(1000);
             jedisPoolConfig.setMaxIdle(-1);
             jedisPoolConfig.setMaxWaitMillis(500);
             jedisPoolConfig.setTestOnBorrow(true);
             jedisPoolConfig.setTestOnReturn(true);
            
             return jedisPoolConfig;
         }
         
         @Bean( name = "redisConnectionFactory" )
         public JedisConnectionFactory redisConnectionFactory() {
        	 String redisHosts=config.getStringValue( "redis.host" );
        	 String hostArr[]=redisHosts.split( "," );
        	 List<String> hosts = Arrays.asList( hostArr);        	 
           JedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory(new RedisClusterConfiguration( hosts ));
            System.out.println( "redisHosts---  :"+hosts );          
           //redisConnectionFactory.setHostName(redisHost);
           //redisConnectionFactory.setPort(redisPort);
           redisConnectionFactory.setPoolConfig( jedisPoolConfig() );
           redisConnectionFactory.setUsePool(true);
           log.info( " RedisConnectionFactory  bean is instantiated.....successfully  : "+redisConnectionFactory );
           return redisConnectionFactory;
           
         }

         @Bean( name = "redisTemplate" )
         public RedisTemplate<String, List<Object>> redisTemplate(RedisConnectionFactory cf) {
           RedisTemplate<String, List<Object>> redisTemplate = new RedisTemplate<String, List<Object>>();
           redisTemplate.setConnectionFactory(cf);
          // redisTemplate.setDefaultSerializer( new StringRedisSerializer() );
           System.out.println( "setKeySerializer...." );
           redisTemplate.setKeySerializer(new StringRedisSerializer());
           //redisTemplate.setValueSerializer(new JsonRedisSerializer());
         // redisTemplate.setValueSerializer(new StringRedisSerializer());
           //redisTemplate.setHashValueSerializer( new GenericToStringSerializer< Object >( Object.class ) );
           //redisTemplate.setValueSerializer( new GenericToStringSerializer< Object >( Object.class ) );
           System.out.println(  " RedisTemplate  bean is instantiated.....successfully  : "+redisTemplate );
           return redisTemplate;
         }

         
         @Bean( name = "stringRedisTemplate" )
         public RedisTemplate<String, String> stringRedisTemplate(RedisConnectionFactory cf) {
           RedisTemplate<String, String> stringRedisTemplate = new RedisTemplate<String, String>();
           stringRedisTemplate.setConnectionFactory(cf);
          // redisTemplate.setDefaultSerializer( new StringRedisSerializer() );
           System.out.println( "setKeySerializer...." );
           stringRedisTemplate.setKeySerializer(new StringRedisSerializer());
           //redisTemplate.setValueSerializer(new JsonRedisSerializer());
         // redisTemplate.setValueSerializer(new StringRedisSerializer());
           //redisTemplate.setHashValueSerializer( new GenericToStringSerializer< Object >( Object.class ) );
           //redisTemplate.setValueSerializer( new GenericToStringSerializer< Object >( Object.class ) );
           log.info( " stringRedisTemplate  bean is instantiated.....successfully  : "+stringRedisTemplate );
           return stringRedisTemplate;
         }
         
         
         @Bean( name = "cacheManager" )
         public CacheManager cacheManager(RedisTemplate redisTemplate) {
           RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);          
           cacheManager.setDefaultExpiration(0);
           log.info( " RedisCacheManager  bean is instantiated.....successfully  : "+cacheManager );
           return cacheManager;
         }
}
