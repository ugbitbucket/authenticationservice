package com.nextwave.utp.authentication.init;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.nextwave.utp.framework.config.Configuration;
import com.nextwave.utp.framework.configuration.propsfile.ZooKeeperConfig;

@Component
@Order(1)
public class ZookeeperInit
{
	private static final Logger log = Logger.getLogger(ZookeeperInit.class);
	private Configuration config;

	@Bean( name = "config" )
	public Configuration config()
	{
		Configuration config = ZooKeeperConfig.getZookeeperConfig( "/home/orion/zookeeper.props" );
		setConfiguration( config );
		log.info( "ZookeeperInit.... config initialized...." );
		return config;
	}

	public Configuration getConfiguration()
	{
		return config;
	}

	public void setConfiguration( Configuration config )
	{
		this.config = config;
	}
}
