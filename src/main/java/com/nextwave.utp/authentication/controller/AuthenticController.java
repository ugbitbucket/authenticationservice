package com.nextwave.utp.authentication.controller;



import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nextwave.utp.authentication.service.dao.AuthenticateServiceDAO;
import com.nextwave.utp.authentication.service.dao.model.PlayerFBDetails;
import com.nextwave.utp.authentication.util.AuthenticationUtil;
import com.nextwave.utp.cache.modelObject.PlayerFBAndDeviceDet;
import com.nextwave.utp.redisCacheManger.RedisGetAndPutUtil;
import com.google.gson.Gson;



@RestController
@RequestMapping( value = "/authentication", produces = MediaType.APPLICATION_JSON )
public class AuthenticController
{

	Gson gson = new Gson();
	@Autowired
	AuthenticateServiceDAO authenticationDAO;
	@Autowired
	AuthenticationUtil util;
	@Autowired
	private RedisGetAndPutUtil redisUtil;
	
	@RequestMapping( value = "checkPlayer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON )
	public String checkPlayer( @RequestBody String jsonObj )
	{
		try
		{
			System.out.println( "Checking player authentication " );
			System.out.println( "jsonObj   : "+jsonObj );
			JSONParser parser = new JSONParser();			
			JSONObject json = (JSONObject) parser.parse(jsonObj);
			System.out.println( " FBuid  :"+json.get( "fbuid" ) );
			System.out.println( " DeviceId  :"+json.get( "deviceId" ) );
			System.out.println( " Playerid  :"+json.get( "playerid" ) );
			PlayerFBAndDeviceDet playerFBAndDeviceDet= util.checkAndGetPlayerFBAndDeviceDetails((String) json.get( "fbuid" ), (String)json.get( "deviceId" )  );
			String JSon=gson.toJson( playerFBAndDeviceDet );
			JSONObject output = (JSONObject)parser.parse( JSon);
			System.out.println( "JsonObj----"+output );
			return output.toJSONString();
		        
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		return "";
	}

	
	
	
}
