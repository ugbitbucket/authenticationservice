package com.nextwave.utp.redisCacheManger;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import com.nextwave.utp.authentication.service.dao.model.PlayerDetails;
import com.nextwave.utp.cache.modelObject.UserDetails;

@Component
public class RedisGetAndPutUtil
{
	@Autowired
	@Qualifier("redisTemplate")
	private RedisTemplate< String, List< Object > > redisTemplate;
	
	@Autowired
	@Qualifier("stringRedisTemplate")
	private RedisTemplate< String, String > stringRedisTemplate;
	/*static final String fbidAppender = "_Fbid";
	static final String deviceIdAppender = "_Did";*/

	
	@SuppressWarnings( "unchecked" )
	public List< Object > getObjectListFromRedis( String key )
	{
		//fbuidKey = fbuidKey + fbidAppender;
		/*System.out.println( "redisTemplate :  "+redisTemplate+"======="+redisTemplate.opsForValue() );
		System.out.println(" Inside Redis--- Values :"+redisTemplate.opsForValue().get( key ) +"  Key :"+key );	*/	
		return redisTemplate.opsForValue().get( key );
	}

	
	public void putDeviceIdAndFbuidKeyIntoRedis( String fbuidKey, String deviceID )
	{
		try
		{
			//fbuidKey = fbuidKey + fbidAppender;
			try
			{
				List< Object > objList = getObjectListFromRedis( fbuidKey );			
				UserDetails userDet=new UserDetails(deviceID,new Timestamp(System.currentTimeMillis()));
				if(objList==null)
				{
					List< UserDetails > deviceIdlist = new ArrayList< UserDetails >();
					deviceIdlist.add( userDet );
					List< Object > objlist = new ArrayList< Object >(deviceIdlist);	
					System.out.println( "Put fbuidKey :"+fbuidKey+"   deviceID :"+deviceID );
					redisTemplate.opsForValue().set( fbuidKey, objlist );
				}
				else if(objList!=null)
				{
					boolean isAvailable = false;
					List<UserDetails> list=new ArrayList<UserDetails>();
					for( Object object : objList )
					{
						UserDetails details = ( UserDetails ) object;
						if( details.getId().equals( deviceID ) )
						{
							isAvailable = true;
							break;
						}
						else
						{
							list.add( details );
						}
					}
					if( !isAvailable )
					{
						UserDetails details = new UserDetails( deviceID, new Timestamp( System.currentTimeMillis() ) );
						list.add( details );
						List<Object> objectList=new ArrayList<Object>(list);
						redisTemplate.opsForValue().set( fbuidKey, objectList );
					}
				}
			}
			catch( Exception e )
			{
				e.printStackTrace();
			}		
			
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	/*@SuppressWarnings( "unchecked" )
	public List< Object > getListOfFbuIdByDeviceIdFromRedis( String deviceIdkey )
	{
		//deviceIdkey=deviceIdkey+deviceIdAppender;
		//return ( List< Object > ) redisTemplate.opsForValue().get( deviceIdkey );
		return ( List< Object > ) redisTemplate.opsForValue().get( deviceIdkey );
		
	}*/

	public void putFbuidAndDeviceIdKeyIntoRedis( String deviceIdkey, String fbuid )
	{
		try
		{
			//deviceIdkey = deviceIdkey + deviceIdAppender;
			try
			{
				//List< Object > fbuidLists = new ArrayList< Object >();
				List< Object > objList = getObjectListFromRedis( deviceIdkey );
				UserDetails userDet=new UserDetails(fbuid,new Timestamp(System.currentTimeMillis()));
				if(objList==null)
				{
					List< UserDetails > deviceIdlist = new ArrayList< UserDetails >();
					deviceIdlist.add( userDet );
					List< Object > objlist = new ArrayList< Object >(deviceIdlist);
					System.out.println( "Put deviceIdkey :"+deviceIdkey+"   fbuid :"+fbuid );
					redisTemplate.opsForValue().set( deviceIdkey, objlist );
				}
				else if(objList!=null)
				{
					boolean isAvailable = false;
					List<UserDetails> list=new ArrayList<UserDetails>();
					for( Object object : objList )
					{
						UserDetails details = ( UserDetails ) object;
						if( details.getId().equals( fbuid ) )
						{
							isAvailable = true;
							break;
						}
						else
						{
							list.add( details );
						}
					}
					if( !isAvailable )
					{
						UserDetails details = new UserDetails( fbuid, new Timestamp( System.currentTimeMillis() ) );
						list.add( details );
						List<Object> objectList=new ArrayList<Object>(list);
						redisTemplate.opsForValue().set( deviceIdkey, objectList );
					}
					
					
				}
			}
			catch( Exception e )
			{
				e.printStackTrace();
			}
			

		}
		catch( Exception e )
		{

			e.printStackTrace();
		}
	}
	// DB data putting on redis cache
	public void putListOfDeviceIdAgainstFbuidKeyIntoRedis(String currentFbuidkey, List<PlayerDetails> playerDetList, String currentDeviceId)
	{		
		try
		{
			List<UserDetails> list=new ArrayList<UserDetails>();
			for( PlayerDetails playerDetails : playerDetList )
			{
				
				UserDetails details = new UserDetails( playerDetails.getDeviceId(), playerDetails.getLastLogin() );
				if(currentFbuidkey.equals( playerDetails.getFbuid() ) && currentDeviceId.equals( playerDetails.getDeviceId() ))
				{
					details.setLastLogin( new Timestamp( System.currentTimeMillis() ) );
				}
				list.add( details );
				
			}		
			List<Object> objectList=new ArrayList<Object>(list);
			redisTemplate.opsForValue().set( currentFbuidkey, objectList );
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
		
	}
	
	// DB data putting on redis cache
	public void putListOfFBuIdAgainstDeviceIdKeyIntoRedis(String currentDeviceIdkey,List<PlayerDetails> playerDetList,String currentFbuid)
	{
		try
		{
			List<UserDetails> list=new ArrayList<UserDetails>();
			for( PlayerDetails playerDetails : playerDetList )
			{
				
				UserDetails details = new UserDetails( playerDetails.getFbuid(), playerDetails.getLastLogin() );
				if(currentDeviceIdkey.equals( playerDetails.getDeviceId() ) && currentFbuid.equals( playerDetails.getFbuid() ))
				{
					details.setLastLogin( new Timestamp( System.currentTimeMillis() ) );
				}
				list.add( details );
				
			}		
			List<Object> objectList=new ArrayList<Object>(list);
			redisTemplate.opsForValue().set( currentDeviceIdkey, objectList );
		}
		catch( Exception e )
		{
			
			e.printStackTrace();
		}
	}
	
	public void updateLastLoginInRedisOnFBKey(String fbuidKey,String deviceId)
	{
		try
		{
			List< Object > objList = getObjectListFromRedis( fbuidKey );			
			boolean isAvailable = false;
			List<UserDetails> list=new ArrayList<UserDetails>();
			if(objList!=null)
			{
				for( Object object : objList )
				{
					UserDetails details = ( UserDetails ) object;
					if( details.getId().equals( deviceId ) )
					{
						isAvailable = true;
						break;
					}
					else
					{
						list.add( details );
					}
				}
				if( isAvailable )
				{
					UserDetails details = new UserDetails( deviceId, new Timestamp( System.currentTimeMillis() ) );
					list.add( details );
					List<Object> objectList=new ArrayList<Object>(list);
					redisTemplate.opsForValue().set( fbuidKey, objectList );
				}
			}
			
			
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}		
	}
	
	public void updateLastLoginInRedisOnDeviceKey(String deviceIdKey,String fbuid)
	{
		try
		{
			List< Object > objList = getObjectListFromRedis( deviceIdKey );			
			boolean isAvailable = false;
			List<UserDetails> list=new ArrayList<UserDetails>();
			if(objList!=null)
			{
				for( Object object : objList )
				{
					UserDetails details = ( UserDetails ) object;
					if( details.getId().equals( fbuid ) )
					{
						isAvailable = true;
						break;
					}
					else
					{
						list.add( details );
					}
				}
				if( isAvailable )
				{
					UserDetails details = new UserDetails( fbuid, new Timestamp( System.currentTimeMillis() ) );
					list.add( details );
					List<Object> objectList=new ArrayList<Object>(list);
					redisTemplate.opsForValue().set( deviceIdKey, objectList );
				}
			}
			
			
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}		
	}
	
	public String getFromRedis( String key )
	{
		return stringRedisTemplate.opsForValue().get( key );
	}
	
	public void putIntoRedis( String key,String value )
	{
	        stringRedisTemplate.opsForValue().set( key, value );
	}
	
}
