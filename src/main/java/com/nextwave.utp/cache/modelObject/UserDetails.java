package com.nextwave.utp.cache.modelObject;

import java.io.Serializable;
import java.sql.Timestamp;

public class UserDetails implements Serializable
{

         // this object is used for Redis cache
	private static final long serialVersionUID = -6879992884005033L;
	private String id;
	private Timestamp lastLogin;
	
	
	
	public UserDetails( String id, Timestamp lastLogin )
	{
		
		this.id = id;
		this.lastLogin = lastLogin;
	}
	
	public String getId()
	{
		return id;
	}
	public void setId( String id )
	{
		this.id = id;
	}
	public Timestamp getLastLogin()
	{
		return lastLogin;
	}
	public void setLastLogin( Timestamp lastLogin )
	{
		this.lastLogin = lastLogin;
	}
	
	@Override
	public String toString()
	{
		return "UserLoginDetails [id=" + id + ", lastLogin=" + lastLogin + "]";
	}
	
	
	
	
}
