package com.nextwave.utp.cache.modelObject;

import java.io.Serializable;


public class PlayerFBAndDeviceDet implements Serializable
{
	
	private static final long serialVersionUID = 12034545100908766l;
	
	private int fbCnt=0;
        private int deviceIdCnt=0;
        private String imageURL;
        private int textId;
        private String text;
        private int fbThresholdVal;
        private int deviceThresholdVal;
        private boolean isLoginAllowed;
        
        
        

	public PlayerFBAndDeviceDet( int fbCnt, int deviceIdCnt, String imageURL, int textId, String text, int fbThresholdVal, int deviceThresholdVal, boolean isLoginAllowed )
	{
		
		this.fbCnt = fbCnt;
		this.deviceIdCnt = deviceIdCnt;
		this.imageURL = imageURL;
		this.textId = textId;
		this.text = text;
		this.fbThresholdVal = fbThresholdVal;
		this.deviceThresholdVal = deviceThresholdVal;
		this.isLoginAllowed = isLoginAllowed;
	}
	



	public PlayerFBAndDeviceDet()
	{
		
	}




	public int getFbCnt()
	{
		return fbCnt;
	}



	public void setFbCnt( int fbCnt )
	{
		this.fbCnt = fbCnt;
	}



	public int getDeviceIdCnt()
	{
		return deviceIdCnt;
	}



	public void setDeviceIdCnt( int deviceIdCnt )
	{
		this.deviceIdCnt = deviceIdCnt;
	}



	public String getImageURL()
	{
		return imageURL;
	}



	public void setImageURL( String imageURL )
	{
		this.imageURL = imageURL;
	}



	public int getTextId()
	{
		return textId;
	}



	public void setTextId( int textId )
	{
		this.textId = textId;
	}



	public String getText()
	{
		return text;
	}



	public void setText( String text )
	{
		this.text = text;
	}



	public int getFbThresholdVal()
	{
		return fbThresholdVal;
	}



	public void setFbThresholdVal( int fbThresholdVal )
	{
		this.fbThresholdVal = fbThresholdVal;
	}



	public int getDeviceThresholdVal()
	{
		return deviceThresholdVal;
	}



	public void setDeviceThresholdVal( int deviceThresholdVal )
	{
		this.deviceThresholdVal = deviceThresholdVal;
	}



	public boolean isLoginAllowed()
	{
		return isLoginAllowed;
	}



	public void setLoginAllowed( boolean isLoginAllowed )
	{
		this.isLoginAllowed = isLoginAllowed;
	}



	@Override
	public String toString()
	{
		return "PlayerFBAndDeviceDet [fbCnt=" + fbCnt + ", deviceIdCnt=" + deviceIdCnt + ", imageURL=" + imageURL + ", textId=" + textId + ", text=" + text + ", fbThresholdVal="
				+ fbThresholdVal + ", deviceThresholdVal=" + deviceThresholdVal + ", isLoginAllowed=" + isLoginAllowed + "]";
	}
	
	
	
	
        
}
