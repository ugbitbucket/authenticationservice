package com.nextwave.utp.mvel.rule.engine;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RuleBean {
	String name;
	String expression;
	String outcome;
	int priority;
	String namespace;
	String description;
	boolean isSubRule;

	public RuleBean() {

	}

	public RuleBean(String name, String expression, String outcome,
			int priority, String namespace, String description,
			boolean isSubRule) {
		super();
		this.name = name;
		this.expression = expression;
		this.outcome = outcome;
		this.priority = priority;
		this.namespace = namespace;
		this.description = description;
		this.isSubRule = isSubRule;
	}

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	public String getExpression() {
		return expression;
	}

	@XmlElement
	public void setExpression(String expression) {
		this.expression = expression;
	}

	public String getOutcome() {
		return outcome;
	}

	@XmlElement
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}

	public int getPriority() {
		return priority;
	}

	@XmlElement
	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getNamespace() {
		return namespace;
	}

	@XmlElement
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getDescription() {
		return description;
	}

	@XmlElement
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSubRule() {
		return isSubRule;
	}

	@XmlElement
	public void setSubRule(boolean isSubRule) {
		this.isSubRule = isSubRule;
	}

	@Override
	public String toString() {
		return "RuleBean [name=" + name + ", expression=" + expression
				+ ", outcome=" + outcome + ", priority=" + priority
				+ ", namespace=" + namespace + ", description=" + description
				+ ", isSubRule=" + isSubRule + "]";
	}
}
